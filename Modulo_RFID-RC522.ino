/*Utilizo el arduino MEGA 2560

Disposicion de pines:
  3.3V ==> 3.3V
  RST  ==> Pin 49
  GND  ==> GND
  IRQ no se conecta
  MISO ==> Pin 50
  MOSI ==> Pin 51
  SCK  ==> Pin 52
  SS/SDA  ==> Pin 53
*/  

#define RST  49
#define MISO 50
#define MOSI 51
#define SCK  52
#define SDA  53

#include <SPI.h>
#include <MFRC522.h>

MFRC522 mfrc522(SDA, RST);  //Instancio la clase

void setup() 
{
  // put your setup code here, to run once:

  Serial.begin (9600);
  SPI.begin();          //Inicializo el SPI
  mfrc522.PCD_Init();   //Init MFRC522
  Serial.println ("Leyendo el RFID");
}

void loop() 
{
  // put your main code here, to run repeatedly:

  if (mfrc522.PICC_IsNewCardPresent())
  {
    if (mfrc522.PICC_ReadCardSerial())
    {
      Serial.print ("Tag UID:");
      for (byte i = 0; i < mfrc522.uid.size; i++)
        {
          Serial.print (mfrc522.uid.uidByte[i] < 0x10 ? "0" : " ");
          Serial.print (mfrc522.uid.uidByte[i], HEX);
        }

        

        Serial.println();
        mfrc522.PICC_HaltA();
    }
  }
}
